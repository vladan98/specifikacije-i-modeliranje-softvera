﻿using Hospital.Model.Enum;
using Hospital.ViewModel;
using Hospital.ViewModel.Util;
using System.Windows;

namespace Hospital
{
    // View - ViewModel pristup ***********************************

    // Svaki ViewModel nasledjuje BindableBase klasu koja implementira INotifyPropertyChanged interfejs
    // sto omogucava Binding izmedju View i ViewModel fajlova (dodeljivanje i osvezavanje vrednosti promenljivih
    // - npr. Text="{Binding LoginErrorText} (LoginView.xaml 26 LOC) -> public string LoginErrorText (LoginViewModel.cs 78 LOC))
    // da bi se vrednost osvezila u View komponenti potrebno je po promeni vrednosti pozvati OnPropertyChanged("LoginErrorText");
    // metodu iz klase BindableBase tj. interfejsa INotifyPropertyChanged

    // *************************************************************
    class MainWindowViewModel : BindableBase
    {
        // Inicijalizacija: ***
        // Reference na aplikaciju
        private readonly App _app;
        // Containera za komponente
        private BindableBase currentViewModel;
        // ** Kontrola: **
        // - Login
        private readonly LoginViewModel loginViewModel;
        // - Glavni meni
        private MainMenuViewModel mainMenuViewModel;
        // - Prikaz lekova
        private DisplayMedicationViewModel displayMedicationViewModel;
        // - Izmena lekova
        private EditMedicationViewModel editMedicationViewModel;
        // - Prikaz recepata
        private DisplayPrescriptionViewModel displayPrescriptionViewModel;
        // - Dodavanje recepata
        private AddPrescriptionViewModel addPrescriptionViewModel;
        // - Registrovanje korisnika
        private RegisterUserViewModel registerUserViewModel;
        // - Prikaz korisnika
        private DisplayUserViewModel displayUserViewModel;
        // - Prikaz izvestaja
        private GenerateReportViewModel generateReportViewModel;
        // - Prikaz izvestaja
        private PurchaseViewModel purchaseViewModel;
        // ** Komanda: **
        // - Glavni meni komanda
        public MyICommand MainMenuCommand { get; set; }
        // - Prodaja lekova
        public MyICommand PurchaseMedicationCommand { get; set; }
        // Korisnik ulogovan
        private bool isLogged;
        // Apotekar ulogovan
        private bool isPharmacist;

        public MainWindowViewModel() {
            //Deklaracija: ***
            // - inicijalne kontrole
            loginViewModel = new LoginViewModel();
            CurrentViewModel = loginViewModel;
            // Komande Glavni meni 
            MainMenuCommand = new MyICommand(MainMenu);
            // Komande Kupovina lekova
            PurchaseMedicationCommand = new MyICommand(PurchaseMedication);
            // Reference na aplikaciju
            _app = Application.Current as App;
        }
        // Prikazivanje glavnog menija
        public void MainMenu()
        {
            // Pozivanje funkcije za navigaciju
            OnNav(NavigationDestination.MAIN_MENU);
        }
        public void PurchaseMedication()
        {
            OnNav(NavigationDestination.PURCHASE_MEDICATION);
        }

        // Poziva se kada se korsnik uloguje u LoginViewModel
        public void LoginSuccess()
        {
            // Provera da li je korisnik ulogovan i tipa korisnika
            IsUserLogged();
            IsUserPharmacist();
        }
        public void IsUserLogged()
        {
            IsLogged = _app.LoggedUser != null;
        }
        public void IsUserPharmacist()
        {
            IsPharmacist = _app.LoggedUser.UserType == UserType.Pharmacist;
        }

        // Navigacija poziva se pri kliku na dugme iz glavnog menija ili pri drugoj potrebi za navigacijom
        // Navigacija se postize promenom sadrzaja (CurrentViewModel) ContentControl kontrole (MainWindow.xaml 70 LOC)
        // Promenom sadrzaja ContentControl-e (promenom DataContext-a) u ContentControl-i postavlja se i 
        // sadrzaj View komponente za koju je vezan taj DataContext (ViewModel fajl koji je vezan za View fajl)
        // ViewModel i View fajlovi povezani su u okviru <DataTemplate> taga (MainWindow.xaml 17 LOC) i
        // u svakom pojedinacnom View fajlu u <UserControl.DataContext> 
        // ****** Pogledati MainWindowViewModel.cs 8 LOC za dalje objasnjenje View - ViewModel pristupa
        public void OnNav(NavigationDestination destination)
        {
            switch (destination)
            {
                case NavigationDestination.MAIN_MENU:
                    // Deklaracija kontrole
                    mainMenuViewModel = new MainMenuViewModel();
                    // Postavljanje kontrole
                    CurrentViewModel = mainMenuViewModel;
                    break;
                case NavigationDestination.DISPLAY_MEDICATION:
                    displayMedicationViewModel = new DisplayMedicationViewModel();
                    CurrentViewModel = displayMedicationViewModel;
                    break;
                case NavigationDestination.PURCHASE_MEDICATION:
                    purchaseViewModel = new PurchaseViewModel();
                    CurrentViewModel = purchaseViewModel;
                    break;
                case NavigationDestination.EDIT_MEDICATION:
                    editMedicationViewModel = new EditMedicationViewModel();
                    CurrentViewModel = editMedicationViewModel;
                    break;
                case NavigationDestination.DISPLAY_PRESCRIPTION:
                    displayPrescriptionViewModel = new DisplayPrescriptionViewModel();
                    CurrentViewModel = displayPrescriptionViewModel;
                    break;
                case NavigationDestination.ADD_PRESCRIPTION:
                    addPrescriptionViewModel = new AddPrescriptionViewModel();
                    CurrentViewModel = addPrescriptionViewModel;
                    break;
                case NavigationDestination.REGISTER_USER:
                    registerUserViewModel = new RegisterUserViewModel();
                    CurrentViewModel = registerUserViewModel;
                    break;
                case NavigationDestination.DISPLAY_USER:
                    displayUserViewModel = new DisplayUserViewModel();
                    CurrentViewModel = displayUserViewModel;
                    break;
                case NavigationDestination.GENERATE_REPORT:
                    generateReportViewModel = new GenerateReportViewModel();
                    CurrentViewModel = generateReportViewModel;
                    break;
            }
        }


        #region Getters & Setters

        public BindableBase CurrentViewModel
        {
        get { return currentViewModel; }
        set
        {
            SetProperty(ref currentViewModel, value);
        }
        }
        public bool IsLogged
        {
            get { return isLogged; }
            set
            {
                if (isLogged != value)
                {
                    isLogged = value;
                    OnPropertyChanged("IsLogged");
                }
            }
        }
        public bool IsPharmacist
        {
            get { return isPharmacist; }
            set
            {
                if (isPharmacist != value)
                {
                    isPharmacist = value;
                    OnPropertyChanged("IsPharmacist");
                }
            }
        }

        #endregion
    }
}
