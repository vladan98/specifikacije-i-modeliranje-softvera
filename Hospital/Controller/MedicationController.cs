﻿using Hospital.Model;
using Hospital.Model.Enum;
using Hospital.Service;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hospital.Controller
{
    public class MedicationController
    {

        private readonly MedicationService _service;

        public MedicationController(MedicationService service)
        {
            _service = service;
        }
        // Kreiraj lek 
        public object Create(Medication medication, User user) 
        {
            // Autorizacija, vrati true ako je ispravno kreiran
            if (user.UserType == UserType.Administrator || user.UserType == UserType.Pharmacist) { _service.Create(medication); return true; }
            else { return "You are not alowed to create medication"; }
        }
        // Izmeni lek 
        public object Update(Medication medication, User user)
        {
            if (user.UserType == UserType.Administrator || user.UserType == UserType.Pharmacist) { _service.Update(medication); return true; }
            else { return "You are not alowed to edit medication"; }
        }
        // Obrisi lek
        public object Delete(Medication medication, User user)
        {
            if (user.UserType == UserType.Administrator || user.UserType == UserType.Pharmacist) { _service.Delete(medication); return true; }
            else { return "You are not alowed to delete medication"; }
        }
        // Nadji lek sa prosledjenim kodom
        public Medication GetOneByCode(string code, User loggedUser) => _service.GetOneByCode(code, loggedUser);
        // Vrati sve lekove
        public IEnumerable<Medication> GetAll(User loggedUser) => _service.GetAll(loggedUser);
        // Vrati sve proizvodjace
        public IEnumerable<string> GetAllManufacturers() => _service.GetAllManufacturers();
        // Pretraga po delu koda
        public IEnumerable<Medication> SearchMedicationByCode(string code, User loggedUser) => _service.SearchMedicationsByCode(code, loggedUser);
        // Pretraga po opsegu cene
        public IEnumerable<Medication> SearchMedicationByPriceRange(float min, float max, User loggedUser) => _service.SearchMedicationsByPriceRange(min, max, loggedUser);
        // Pretraga po delu imena
        public IEnumerable<Medication> SearchMedicationByName(string name, User loggedUser) => _service.SearchMedicationsByName(name, loggedUser);
        // Pretraga po delu imena proizvodjaca
        public IEnumerable<Medication> SearchMedicationByManufacturer(string manufacturer, User loggedUser) => _service.SearchMedicationsByManufacturer(manufacturer, loggedUser);
        // Pretraga po imenu
        public IEnumerable<Medication> SortMedicationByName(List<Medication> medications) => _service.SortMedicationByName(medications);
        // Pretraga po proizvodjacu
        public IEnumerable<Medication> SortMedicationByManufacturer(List<Medication> medications) => _service.SortMedicationByManufacturer(medications);
        // Pretraga po ceni
        public IEnumerable<Medication> SortMedicationByPrice(List<Medication> medications) => _service.SortMedicationByPrice(medications);
    }
}
