﻿using Hospital.Model;
using Hospital.Model.Enum;
using Hospital.Service;
using System.Collections.Generic;

namespace Hospital.Controller
{
    public class PrescriptionController
    {

        private readonly PrescriptionService _service;

        public PrescriptionController(PrescriptionService service)
        {
            _service = service;
        }
        // Kreiranje recepta
        public object Create(Prescription prescription, User loggedUser) {
            // Autorizacija
            if(loggedUser.UserType == UserType.Doctor) return _service.Create(prescription);
            else { return "You are not allowed to create prescription"; };
        } 
        // vrati sve recepte
        public IEnumerable<Prescription> GetAll() => _service.GetAll();
        // Izvuci spisak lekova iz recepta
        public IEnumerable<Medication> GetAllMedicationFromPrescription(int prescriptionCode) => _service.GetAllMedicationFromPrescription(prescriptionCode);
        // Sortiranje po kodu
        public IEnumerable<Prescription> SortPrescriptionByCode(IEnumerable<Prescription> prescriptions) => _service.SortPrescriptionByCode(prescriptions);
        // Sortiranje po doktoru
        public IEnumerable<Prescription> SortPrescriptionByDoctor(IEnumerable<Prescription> prescriptions) => _service.SortPrescriptionByDoctor(prescriptions);
        // Sortiranje po datumu
        public List<Prescription> SortPrescriptionByDate(List<Prescription> prescriptions) => _service.SortPrescriptionByDate(prescriptions);
        // Pretraga po kodu
        public IEnumerable<Prescription> SearchPrescriptionByCode(int code) => _service.SearchPrescriptionByCode(code);
        // Pretraga po delu imena doktora
        public IEnumerable<Prescription> SearchPrescriptionByDoctor(string doctorNamePart) => _service.SearchPrescriptionByDoctor(doctorNamePart);
        // Pretraga po delu JMBG-a
        public IEnumerable<Prescription> SearchPrescriptionByJMBG(string jmbgPart) => _service.SearchPrescriptionByJMBG(jmbgPart);
        // Pretraga po delu imena leka
        public IEnumerable<Prescription> SearchPrescriptionByMedication(string medicationNamePart) => _service.SearchPrescriptionByMedication(medicationNamePart);
    }
}
