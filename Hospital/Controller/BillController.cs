﻿using Hospital.Model;
using Hospital.Model.Enum;
using Hospital.Service;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hospital.Controller
{
    public class BillController
    {
        private readonly BillService _service;
        
        public BillController(BillService service)
        {
            _service = service;
        }
        // Kreiranje racuna
        public object Create(User loggedUser,Bill bill)
        {
            // Autorizacija, vrati true ako je ispravno kreiran
            if (loggedUser.UserType == UserType.Pharmacist) { _service.Create(bill); return true; }
            else { return "You are not authorised to create bills"; }
        }
        // Izvlacenje lekova sa informacijama o prodaji
        public object GetAllMedicationWithSellInfo(User loggedUser)
        {
            // Vrati lekove ako je korisnik autorizovan
            if (loggedUser.UserType == UserType.Administrator) { return _service.GetAllMedicationWithSellInfo(); }
            else { return "You are not authorised to view sell info"; }
        }
        // Izvlacenje lekova sa informacijama o prodaji na osnovu proizvodjaca
        public object GetMedicationWithSellInfoByManufacturer(string manufacturer,User loggedUser)
        {
            // Vrati lekove ako je korisnik autorizovan
            if (loggedUser.UserType == UserType.Administrator) { return _service.GetMedicationWithSellInfoByManufacturer(manufacturer); }
            else { return "You are not authorised to view sell info"; }
        }
        // Izvlacenje lekova sa informacijama o prodaji na osnovu proizvodjaca
        public object GetMedicationWithSellInfoByPharmacist(User pharmacist, User loggedUser)
        {
            // Vrati lekove ako je korisnik autorizovan
            if (loggedUser.UserType == UserType.Administrator) { return _service.GetMedicationWithSellInfoByPharmacist(pharmacist); }
            else { return "You are not authorised to view sell info"; }
        }
        
    }
}
