﻿using Hospital.Model;
using Hospital.Model.Enum;
using Hospital.Service;
using System.Collections.Generic;

namespace Hospital.Controller
{
    public class UserController
    {
    
        private readonly UserService _service;

        public UserController(UserService service)
        {
            _service = service;
        }
        // Logovanje
        public object TryLogin(string username, string password) => _service.TryLogin(username, password);
        public object GetUserByUsername(string username, User loggedUser) { 
            if(loggedUser.UserType == UserType.Administrator)
            {
                return _service.GetUserByUsername(username);
            }
            else return "You are not authorised to view users";
        }
        // Vrati sve farmaceute
        public object GetAllPharmacists(User loggedUser)
        { 
            if(loggedUser.UserType == UserType.Administrator)
            {
                return _service.GetAllPharmacists();
            }
            else return "You are not authorised to view users";
        }
        // Vrati sve korisnike
        public object GetAll(User loggedUser)
        { 
            if(loggedUser.UserType == UserType.Administrator)
            {
                return _service.GetAll();
            }
            else return "You are not authorised to view users";
        }
        // Kreiraj novog korisnika
        public object Create(User user,User loggedUser)
        {
            // Autorizacija
            if (loggedUser.UserType == UserType.Administrator) { _service.Create(user); return true; }
            else return "You are not authorised to create users";
        }
        // Sortiranje po imenu
        public List<User> SortUsersByName(List<User> users) => _service.SortUsersByName(users);
        // Sortiranje po prezimenu
        public List<User> SortUsersByLastName(List<User> users) => _service.SortUsersByLastName(users);
        // Sortiranje po ulozi
        public List<User> SortUsersByRole(List<User> users) => _service.SortUsersByRole(users);

    }
}
