﻿using Hospital.Model;
using Hospital.Repository.JSONStream;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Hospital.Repository
{
    public class BillRepository
    {
        protected readonly JSONStream<Bill> _stream;
        protected readonly MedicationRepository _medicationRepository;

        public BillRepository(JSONStream<Bill> stream,MedicationRepository medicationRepository)
        {
            _stream = stream;
            _medicationRepository = medicationRepository;
        }
        // Napravi novi racun
        public Bill Create(Bill bill) {
            // Vrati sve racune
            var bills = _stream.ReadAll().ToList();
            // Izracunaj duzinu niza (vrednosti pocinju od 1)
            int lenght = bills.Count;
            // Upisi datum pravljenja racuna
            bill.DateTime = DateTime.Now.ToString("MM/dd/yyyy H:mm");
            // Dodaj sifru za jedan vecu od poslednje
            bill.Code = lenght + 1;
            // Unesi racun u fajl
            _stream.AppendToFile(bill);
            return bill;
        }
        // Svi racuni 
        public IEnumerable<Bill> GetAll() => _stream.ReadAll();
        // Svi lekovi sa info o prodaji
        public IEnumerable<MedicationWithSellInfo> GetAllMedicationWithSellInfo() => GetMedicationWithSellInfo(null);
        // Svi lekovi sa info o prodaji na osnovu prosledjenog proizvodjaca
        public IEnumerable<MedicationWithSellInfo> GetMedicationWithSellInfoByManufacturer(string manufacturer)
        {
            List<MedicationWithSellInfo> all = GetMedicationWithSellInfo(null).ToList();
            return all.FindAll(med => med.Manufacturer == manufacturer);
        }
        // Svi lekovi sa info o prodaji na osnovu prosledjenog farmaceuta
        public IEnumerable<MedicationWithSellInfo> GetMedicationWithSellInfoByPharmacist(User pharmacist) => GetMedicationWithSellInfo(pharmacist).ToList();
        // Lekovi sa podacima o prodaji
        private IEnumerable<MedicationWithSellInfo> GetMedicationWithSellInfo(User pharmacist)
        {
            // Uzmi sve racune
            List<Bill> bills = GetAll().ToList();
            // Ako je prosledjen farmaceut filtriraj samo njegove racune
            if (pharmacist != null) bills = bills.FindAll(bill => bill.Pharmacist == pharmacist.Username);
            // Saberi informacije o prodaji
            Dictionary<string, int> soldMeddications = GetTotalSoldForEachMedication(bills);
            // Povezi lekove sa informacijama o prodaji
            return BindMedicationsToSellInfo(soldMeddications);
            
        }
        // Ukupno prodatnih lekova na osnovu prosledjenih racuna
        private Dictionary<string, int> GetTotalSoldForEachMedication(IEnumerable<Bill> bills)
        {
            // Recnik koda leka i ukupne prodatih
            Dictionary<string, int> soldMeddications = new Dictionary<string, int>();
            // Petlja za sabiranje ukupne prodaje za pojedinacne lekove
            bills.ToList().ForEach(bill =>
            {
                foreach (var medAmount in bill.MedicationAmount)
                {
                    // Ako lista vec sadrzi lek dodaj broj prodatih, u suprotnom unesi lek u listu
                    if (soldMeddications.ContainsKey(medAmount.Key)) soldMeddications[medAmount.Key] += medAmount.Value;
                    else soldMeddications[medAmount.Key] = medAmount.Value;
                }

            });
            return soldMeddications;
        }
        // Povezi lekove sa informacijama o prodaji
        private List<MedicationWithSellInfo> BindMedicationsToSellInfo(Dictionary<string, int> soldMeddications)
        {
            // lista sa lekovima koji sadrze informacije o prodaji
            List<MedicationWithSellInfo> meddicationsWithSellInfo = new List<MedicationWithSellInfo>();
            // petlja za sastavljanje lekova sa informacijama o prodaji
            foreach (var medSumAmount in soldMeddications)
            {
                var medicationData = _medicationRepository.GetOne(medSumAmount.Key);
                if (medicationData != null)
                {
                    var medToAdd = new MedicationWithSellInfo
                    {
                        Code = medicationData.Code,
                        Name = medicationData.Name,
                        Manufacturer = medicationData.Manufacturer,
                        Prescription = medicationData.Prescription,
                        Price = medicationData.Price,
                        Sold = medSumAmount.Value
                    };

                    meddicationsWithSellInfo.Add(medToAdd);
                }
            }
            return meddicationsWithSellInfo;
        }

    }
}
