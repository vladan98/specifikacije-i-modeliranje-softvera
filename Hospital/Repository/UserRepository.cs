﻿using Hospital.Model;
using Hospital.Model.Enum;
using Hospital.Repository.JSONStream;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Hospital.Repository
{
    public class UserRepository
    {
        private const string NOT_FOUND_ERROR = "{0} with {1}:{2} can not be found!";
        protected readonly JSONStream<User> _stream;

        public UserRepository(JSONStream<User> stream)
        {
            _stream = stream;
        }

        public void Create(User user) => _stream.AppendToFile(user);

        // Pronadji korisnika na osnovu korisnickog imena
        public User GetUserByUsername(string username)
        {
            try
            {
                return _stream
                    .ReadAll()
                        .SingleOrDefault(usr => username.CompareTo(usr.Username) == 0);
            }
            catch (ArgumentException)
            {
                throw new Exception(string.Format(NOT_FOUND_ERROR,  "User", "Username", username));
            }
        }
        // Vrati sve korisnike
        public IEnumerable<User> GetAll() => _stream.ReadAll();
        // Vrati sve farmaceute
        public IEnumerable<User> GetAllPharmacists() => _stream.ReadAll().ToList().FindAll(user => user.UserType.Equals(UserType.Pharmacist));
        // Pronadji korisnika na osnovu prosledjene funkcije
        public IEnumerable<User> Find(Func<User, bool> predicate)
            => _stream
            .ReadAll()
            .Where(predicate);


    }
}
