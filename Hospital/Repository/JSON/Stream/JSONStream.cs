﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Hospital.Repository.JSONStream
{
    public class JSONStream<E>
    {
        private readonly string _path;

        public JSONStream(string path)
        {
            _path = path;
        }

        // Sačuvaj sve entitete u JSON fajl (_path)
        public void SaveAll(IEnumerable<E> entities) =>
            File.WriteAllText(_path, JsonConvert.SerializeObject(entities).ToString());


        // Pročitaj sve entitete iz JSON fajla (_path)
        public IEnumerable<E> ReadAll()
        {
            string str = File.ReadAllText(_path);
            return JsonConvert.DeserializeObject<List<E>>(str);
        }

        // Dodaj entitet
        public void AppendToFile(E entity) {
            IEnumerable<E> entities = ReadAll();
            SaveAll(entities.Concat(new[] { entity }));
        }
    }
}
