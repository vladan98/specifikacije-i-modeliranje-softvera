﻿using Hospital.Model;
using Hospital.Repository.JSONStream;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Hospital.Repository
{
    public class PrescriptionRepository
    {
        private const string NOT_FOUND_ERROR = "{0} with {1}:{2} can not be found!";
        private readonly JSONStream<Prescription> _stream;
        private readonly MedicationRepository _medicationRepository;
        private enum FilterParameter
        {
            All,
            Manufacturer,
            Pharmacists,
        }
        public PrescriptionRepository(JSONStream<Prescription> stream, MedicationRepository medicationRepository)
        {
            _stream = stream;
            _medicationRepository = medicationRepository;
        }

        // Napravi novi recept
        public Prescription Create(Prescription prescription)
        {
            var prescriptions = _stream.ReadAll().ToList();
            // izracunaj duzinu niza (vrednosti pocinju od 1)
            int lenght = prescriptions.Count;
            // dodaj sifru za jedan vecu od poslednje
            prescription.Code = lenght + 1;
            // unesi racun u fajl
            _stream.AppendToFile(prescription);
            return prescription;
        }
        // Obrisi recept
        public void Delete(Prescription prescription)
        {
            var prescriptions = _stream.ReadAll().ToList();
            //TODO: Pronadji element i obrisi ga ako postoji
            var prescriptionToRemove = prescriptions.SingleOrDefault(usr => prescription.Code.CompareTo(usr.Code) == 0);
            if (prescriptionToRemove != null)
            {
                prescriptions.Remove(prescriptionToRemove);
                _stream.SaveAll(prescriptions);
            }
            else
            {
                throw new Exception(string.Format(NOT_FOUND_ERROR, "Code", "User", prescription));
            }
        }
        // Vrati sve recepte
        public IEnumerable<Prescription> GetAll() => _stream.ReadAll();
        // Nadji recept na osnovu koda
        public Prescription GetOne(int code)
        {
            try
            {
                return _stream
                    .ReadAll()
                        .SingleOrDefault(usr => code.CompareTo(usr.Code) == 0);
            }
            catch (ArgumentException)
            {
                throw new Exception(string.Format(NOT_FOUND_ERROR, "Prescription", "Code", code));
            }
        }
        // Pronadji recepte na osnovu imena
        public IEnumerable<Prescription> GetPrescriptionsByMedicationNamePart(string medicationNamePart) => FilterPrescriptionsByMedicationNamePart(GetAll(), medicationNamePart);

        // Vrati sve lekove iz recepta
        public IEnumerable<Medication> GetAllMedicationFromPrescription(int prescriptionCode)
        {
            // Pronadji recept
            var prescription = GetOne(prescriptionCode); 
            if (prescription != null)
            {
                // Ako recept postoji izvuci svaki lek
                List<Medication> medications = new List<Medication>();
                foreach (var medAmount in prescription.MedicationAmount) { medications.Add(_medicationRepository.GetOne(medAmount.Key)); }
                return medications;
            } 
            else { return null; }
        }

        // Pronadji elemente na osnovu prosledjene funkcije
        public IEnumerable<Prescription> Find(Func<Prescription, bool> predicate)
            => _stream
                .ReadAll()
                .Where(predicate);
        
        // Filtriraj recepte na osnovu imena leka
        private IEnumerable<Prescription> FilterPrescriptionsByMedicationNamePart(IEnumerable<Prescription> prescriptions, string medNamePart) =>
            prescriptions
               .ToList()
               .FindAll(prescription => {
                   var flag = false;
                   foreach (var medAmount in prescription.MedicationAmount)
                   {
                       var med = _medicationRepository.GetOne(medAmount.Key);
                       if(med != null) flag = ContainsIgnoreCase(med.Name, medNamePart);
                   }
                    return flag;
               });
        // Uporedi stringove
        private bool ContainsIgnoreCase(string value, string valuePart) => value.IndexOf(valuePart, StringComparison.CurrentCultureIgnoreCase) >= 0;

    }

}
