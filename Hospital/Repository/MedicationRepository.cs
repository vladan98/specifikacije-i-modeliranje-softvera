﻿
using Hospital.Model;
using Hospital.Repository.JSONStream;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Hospital.Repository
{
    public class MedicationRepository
    {
        private const string NOT_FOUND_ERROR = "{0} with {1}:{2} can not be found!";
        protected readonly JSONStream<Medication> _stream;

        public MedicationRepository(JSONStream<Medication> stream)
        {
            _stream = stream;
        }
        // Kreiraj novi
        public void Create(Medication medication) => _stream.AppendToFile(medication);
        // Obrisi lek
        public void Delete(Medication medication)
        {
            var medications = _stream.ReadAll().ToList();
            // Nadji lek za brisanje
            var medicationToDelete = medications.SingleOrDefault(med => medication.Code.CompareTo(med.Code) == 0);
            // Proveri da li lek postoji, ako postoji promeni mu Delete flag i sacuvaj sve
            if (medicationToDelete != null)
            {
                medications.Remove(medicationToDelete);
                medicationToDelete.Deleted = true;
                medications.Add(medicationToDelete);
                _stream.SaveAll(medications);
            }
            else {
                // Ako lek nije pronadjen baci gresku
                throw new Exception(string.Format(NOT_FOUND_ERROR, "Code", "Medication", medication));
            }
        }
        // Pronadji lek na osnovu koda
        public Medication GetOne(string code)
        {
            try
            {
                return _stream
                    .ReadAll()
                        .SingleOrDefault(usr => code.CompareTo(usr.Code) == 0);
            }
            catch (ArgumentException)
            {
                // Ako lek nije pronadjen baci gresku
                throw new Exception(string.Format(NOT_FOUND_ERROR, "Medication", "Code", code));
            }
        }
        // Izmeni lek 
        public void Update(Medication medication)
        {
            try
            {
                var entities = _stream.ReadAll().ToList();
                // Pronadji lek i izmeni ga
                entities[entities.FindIndex(med => medication.Code.CompareTo(med.Code) == 0)] = medication;
                // Sacuvaj
                _stream.SaveAll(entities);
            }
            catch (ArgumentException)
            {
                throw new Exception(string.Format(NOT_FOUND_ERROR, "Medication", "Code", medication));
            }
        }
        // Vrati sve lekove
        public IEnumerable<Medication> GetAll() => _stream.ReadAll();
        // Pronadji lek na osnovu prosledjenog predikata
        public IEnumerable<Medication> Find(Func<Medication, bool> predicate)
            => _stream
                .ReadAll()
                .Where(predicate);
        // Vrati sve proizvodjace
        public IEnumerable<string> GetAllManufacturers()
        {
            var allMedications = _stream.ReadAll();
            // petlja za pronalazenje jedinstvenih proizvodjaca
            List<string> allManufacturers = new List<string>();
            allMedications.ToList().ForEach(med =>
            {
                // ako lista vec sadrzi proizvodjaca ne radi nista, u suprotnom unesi proizvodjaca
                if (!allManufacturers.Contains(med.Manufacturer))
                {
                    allManufacturers.Add(med.Manufacturer);
                }
            });
            return allManufacturers;
        }


    }
}
