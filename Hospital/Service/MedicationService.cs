﻿using Hospital.Model;
using Hospital.Model.Enum;
using Hospital.Repository;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace Hospital.Service
{
    public class MedicationService
    {
        private readonly MedicationRepository _repository;
        public MedicationService(MedicationRepository repository)
        {
            _repository = repository;
        }
        // Kreiranje leka
        public void Create(Medication medication) => _repository.Create(medication);
        // Izmena leka
        public void Update(Medication medication) => _repository.Update(medication);
        // Brisanje leka
        public void Delete(Medication medication) => _repository.Delete(medication);
        // Vrati lek sa kodom
        public Medication GetOneByCode(string code, User loggedUser)
        {
            // Proveri da li je korisnik administrator, ako nije vrati mu samo lekove koji nisu obrisani
            var medication = _repository.GetOne(code);
            if (loggedUser.UserType != UserType.Administrator && medication != null && medication.Deleted) return null;
            else return medication;
        }
        // Vrati sve lekove
        public IEnumerable<Medication> GetAll(User loggedUser)
        {
            // Proveri da li je korisnik administrator, ako nije vrati mu samo lekove koji nisu obrisani
            var all = _repository.GetAll();
            if (loggedUser.UserType != UserType.Administrator) return all.ToList().FindAll(med => !med.Deleted);
            else return all;
        }
        // Vrati sve proizvodjace
        public IEnumerable<string> GetAllManufacturers() => _repository.GetAllManufacturers();
        // Pretraga po kodu
        public IEnumerable<Medication> SearchMedicationsByCode(string codePart, User loggedUser)
        {
            // Proveri da li je korisnik administrator, ako nije vrati mu samo lekove koji nisu obrisani
            var all = _repository.Find(m => ContainsIgnoreCase(m.Code, codePart));
            if (loggedUser.UserType != UserType.Administrator) return all.ToList().FindAll(med => !med.Deleted);
            else return all;
        }
        // Pretraga po imenu
        public IEnumerable<Medication> SearchMedicationsByName(string namePart, User loggedUser)
        {
            // Proveri da li je korisnik administrator, ako nije vrati mu samo lekove koji nisu obrisani
            var all = _repository.Find(m => ContainsIgnoreCase(m.Name, namePart)); 
            if (loggedUser.UserType != UserType.Administrator) return all.ToList().FindAll(med => !med.Deleted);
            else return all;
        }
        // Pretraga po proizvodjacu
        public IEnumerable<Medication> SearchMedicationsByManufacturer(string manufacturerPart, User loggedUser)
        {
            // Proveri da li je korisnik administrator, ako nije vrati mu samo lekove koji nisu obrisani
            var all = _repository.Find(m => ContainsIgnoreCase(m.Manufacturer, manufacturerPart));
            if (loggedUser.UserType != UserType.Administrator) return all.ToList().FindAll(med => !med.Deleted);
            else return all;
        }
        // Pretraga po opsegu cene        
        public IEnumerable<Medication> SearchMedicationsByPriceRange(float min, float max, User loggedUser)
        {
            // Proveri da li je korisnik administrator, ako nije vrati mu samo lekove koji nisu obrisani
            var all = _repository.Find(m => IsInRange(min, max, m.Price));
            if (loggedUser.UserType != UserType.Administrator) return all.ToList().FindAll(med => !med.Deleted);
            else return all;
        }
        // Sortiranje po imenu
        public IEnumerable<Medication> SortMedicationByName(IEnumerable<Medication> medications) { var meds = medications.ToList(); meds.Sort(CompareByName); return meds; }
        // Sortiranje po proizvodjacu
        public IEnumerable<Medication> SortMedicationByManufacturer(IEnumerable<Medication> medications) { var meds = medications.ToList(); meds.Sort(CompareByManufacturer); return meds; }
        // Sortiranje po ceni
        public IEnumerable<Medication> SortMedicationByPrice(IEnumerable<Medication> medications) { var meds = medications.ToList(); meds.Sort(CompareByPrice); return meds; }
        // Poredjenje po imenu
        private int CompareByName(Medication m1, Medication m2) => m1.Name.CompareTo(m2.Name);
        // Poredjenje po proizvodjacu
        private int CompareByManufacturer(Medication m1, Medication m2) => m1.Manufacturer.CompareTo(m2.Manufacturer);
        // Poredjenje po ceni
        private int CompareByPrice(Medication m1, Medication m2) => m1.Price.CompareTo(m2.Price);
        // Poredjenje po stringova
        private bool ContainsIgnoreCase(string value, string valuePart) => value.IndexOf(valuePart, StringComparison.CurrentCultureIgnoreCase) >= 0;
        // Provera opsega
        private bool IsInRange(float min, float max,float compare) => compare >= min && compare <= max;
    }
}
