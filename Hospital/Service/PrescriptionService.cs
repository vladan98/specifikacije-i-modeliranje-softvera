﻿using Hospital.Model;
using Hospital.Repository;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace Hospital.Service
{
    public class PrescriptionService
    {
        private readonly PrescriptionRepository _repository;
        public PrescriptionService(PrescriptionRepository repository)
        {
            _repository = repository;
        }
        // Kreiranje recepta
        public Prescription Create(Prescription prescription) => _repository.Create(prescription);
        // Vrati sve 
        public IEnumerable<Prescription> GetAll() => _repository.GetAll();
        // Pretraga po kodu
        public IEnumerable<Prescription> SearchPrescriptionByCode(int codePart) => _repository.Find(p =>ContainsDigit(p.Code, codePart));
        // Pretraga po doktoru
        public IEnumerable<Prescription> SearchPrescriptionByDoctor(string doctorNamePart) => _repository.Find(p =>ContainsIgnoreCase(p.Doctor, doctorNamePart));
        // Pretraga po JMBG
        public IEnumerable<Prescription> SearchPrescriptionByJMBG(string jmbgPart) => _repository.Find(p =>ContainsIgnoreCase(p.JMBG, jmbgPart));
        // Pretraga po leku
        public IEnumerable<Prescription> SearchPrescriptionByMedication(string medicationNamePart) => _repository.GetPrescriptionsByMedicationNamePart(medicationNamePart);
        // Vracanje lekova na osnovu koda recepta
        public IEnumerable<Medication> GetAllMedicationFromPrescription(int prescriptionCode) => _repository.GetAllMedicationFromPrescription(prescriptionCode);
        // Sortiranje po kodu
        public List<Prescription> SortPrescriptionByCode(IEnumerable<Prescription> medications) { var meds = medications.ToList(); meds.Sort(CompareByCode); return meds; }
        // Sortiranje po doktoru
        public List<Prescription> SortPrescriptionByDoctor(IEnumerable<Prescription> medications) { var meds = medications.ToList(); meds.Sort(CompareByDoctor); return meds; }
        // Sortiranje po datumu
        public List<Prescription> SortPrescriptionByDate(IEnumerable<Prescription> medications) { var meds = medications.ToList(); meds.Sort(CompareByDate); return meds; }
        // Uporedi kodove
        private int CompareByCode(Prescription m1, Prescription m2) => m1.Code.CompareTo(m2.Code);
        // Uporedi doktore
        private int CompareByDoctor(Prescription m1, Prescription m2) => m1.Doctor.CompareTo(m2.Doctor);
        // Uporedi datume
        private int CompareByDate(Prescription m1, Prescription m2) => DateTime.Compare(ConvertDate(m1.Date), ConvertDate(m2.Date));
        // Konvertuj datum
        private DateTime ConvertDate(string d) => DateTime.ParseExact(d, "MM/dd/yyyy H:mm", CultureInfo.InvariantCulture);
        // Uporedi stringove
        private bool ContainsIgnoreCase(string value, string valuePart) => value.IndexOf(valuePart, StringComparison.CurrentCultureIgnoreCase) >= 0;
        // Uporedi brojeve
        private bool ContainsDigit(int value, int valuePart) => value.ToString().Contains(valuePart.ToString());

       
    }
}
