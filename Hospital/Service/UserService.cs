﻿using Hospital.Model;
using Hospital.Repository;
using System.Collections.Generic;

namespace Hospital.Service
{
    public class UserService
    {
        private readonly UserRepository _repository;
        public UserService(UserRepository repository)
        {
            _repository = repository;
        }

        // Novi korisnik
        public void Create(User user) => _repository.Create(user);
        // Korisnik na osnovu korisnickog imena
        public User GetUserByUsername(string username) => _repository.GetUserByUsername(username);
        // Svi korisnici
        public IEnumerable<User> GetAll() => _repository.GetAll();
        // Login
        public object TryLogin(string username,string password) {
            User user = _repository.GetUserByUsername(username);
            if (user is null)
            {
                return string.Format("User with username: {0} doesn't exist.", username);
            }
            if (user.Password == password)
            {
                return user;
            }
            else
            {
                return "Invalid Password";
            }
        }
        public IEnumerable<User> GetAllPharmacists() => _repository.GetAllPharmacists();
        // Sortiranje po imenu
        public List<User> SortUsersByName(List<User> users) { users.Sort(CompareByName); return users; }
        // Sortiranje po prezimenu
        public List<User> SortUsersByLastName(List<User> users) { users.Sort(CompareByLastName); return users; }
        // Sortiranje po ulozi
        public List<User> SortUsersByRole(List<User> users) { users.Sort(CompareByUserType); return users; }

        // Uporedi po imenu
        private int CompareByName(User u1, User u2) => u1.Name.CompareTo(u2.Name);
        // Uporedi po prezimenu
        private int CompareByLastName(User u1, User u2) => u1.LastName.CompareTo(u2.LastName);
        // Uporedi po ulozi
        private int CompareByUserType(User u1, User u2) => u1.UserType.CompareTo(u2.UserType);
    }
}
