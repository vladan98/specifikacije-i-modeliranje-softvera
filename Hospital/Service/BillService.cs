﻿using Hospital.Model;
using Hospital.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hospital.Service
{
    public class BillService
    {
        private readonly BillRepository _repository;
        public BillService(BillRepository repository)
        {
            _repository = repository;
        }
        // Kreiranje racuna
        public Bill Create(Bill bill) => _repository.Create(bill);
        // Vrati sve lekove sa info o prodaji
        public IEnumerable<MedicationWithSellInfo> GetAllMedicationWithSellInfo() => _repository.GetAllMedicationWithSellInfo();
        // Vrati lekove na osnovu prosledjenog proizvodjaca sa info o prodaji
        public IEnumerable<MedicationWithSellInfo> GetMedicationWithSellInfoByManufacturer(string manufacturer) => _repository.GetMedicationWithSellInfoByManufacturer(manufacturer);
        // Vrati lekove na osnovu prosledjenog farmaceuta sa info o prodaji
        public IEnumerable<MedicationWithSellInfo> GetMedicationWithSellInfoByPharmacist(User pharmacist) => _repository.GetMedicationWithSellInfoByPharmacist(pharmacist);


    }
}
