﻿using Hospital.Model;
using Hospital.Model.Enum;
using Hospital.ViewModel.Util;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows;

namespace Hospital.ViewModel
{
    class RegisterUserViewModel : BindableBase
    {
        private readonly App _app;

        private string usernameValue="";
        private string firstNameValue;
        private string lastNameValue;
        private string passwordValue;
        private UserType userTypeValue;
        private string registerErrorText = "";

        public ObservableCollection<UserType> userTypes;
        public MyICommand RegisterUserCommand { get; set; }
        public MyICommand SearchPrescriptionCommand { get; set; }


        public RegisterUserViewModel()
        {
            _app = Application.Current as App;
            RegisterUserCommand = new MyICommand(RegisterUser, IsUsernameUnique);

            UserTypes = new ObservableCollection<UserType>();
            AddUserTypes();            
        }

        public void RegisterUser() {
            var user = new User
            {
                Username = UsernameValue,
                Name = FirstNameValue,
                LastName = LastNameValue,
                Password = PasswordValue,
                UserType = UserTypeValue
            };
            _app.UserController.Create(user,_app.LoggedUser);

        }

        public void AddUserTypes(){
            UserTypes.Add(UserType.Doctor);
            UserTypes.Add(UserType.Pharmacist);
        }
        public bool IsUsernameUnique(){
            RegisterErrorText = "";
            if (UsernameValue == "")
            {
                RegisterErrorText = "You can not register user without username.";
                return false;
            }else
            {
                var o = _app.UserController.GetUserByUsername(UsernameValue, _app.LoggedUser);
                if(o != null && !(o is string))
                {
                    RegisterErrorText = "User with username: " + UsernameValue + " already exists";
                    return false;
                }
                else
                {
                    RegisterErrorText = "User created.";
                    EmptyFields();
                    return true;
                }

            }
        }

        public void EmptyFields() {
            PasswordValue = "";
            LastNameValue = "";
            FirstNameValue = "";
            UsernameValue = "";
        }
        #region Getters & Setters
        public UserType UserTypeValue
        {
            get { return userTypeValue; }
            set
            {
                if (userTypeValue != value)
                {
                    userTypeValue = value;
                    OnPropertyChanged("UserTypeValue");
                }
            }
        }
        public string PasswordValue
        {
            get { return passwordValue; }
            set
            {
                if (passwordValue != value)
                {
                    passwordValue = value;
                    OnPropertyChanged("PasswordValue");
                }
            }
        }
        public string LastNameValue
        {
            get { return lastNameValue; }
            set
            {
                if (lastNameValue != value)
                {
                    lastNameValue = value;
                    OnPropertyChanged("LastNameValue");
                }
            }
        }
        public string FirstNameValue
        {
            get { return firstNameValue; }
            set
            {
                if (firstNameValue != value)
                {
                    firstNameValue = value;
                    OnPropertyChanged("FirstNameValue");
                }
            }
        }
        public string UsernameValue
        {
            get { return usernameValue; }
            set
            {
                if (usernameValue != value)
                {
                    usernameValue = value;
                    OnPropertyChanged("UsernameValue");
                    RegisterUserCommand.RaiseCanExecuteChanged();
                }
            }
        }
        public string RegisterErrorText
        {
            get { return registerErrorText; }
            set
            {
                if (registerErrorText != value)
                {
                    registerErrorText = value;
                    OnPropertyChanged("RegisterErrorText");
                }
            }
        }

        public ObservableCollection<UserType> UserTypes
        {
            get
            {
                return userTypes;
            }
            set
            {
                userTypes = value;
                OnPropertyChanged("UserTypes");
            }
        }

        #endregion
    }
}
