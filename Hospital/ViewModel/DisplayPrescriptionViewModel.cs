﻿using Hospital.Model;
using Hospital.ViewModel.Util;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;

namespace Hospital.ViewModel
{
    class DisplayPrescriptionViewModel : BindableBase
    {
        private readonly App _app;
        private readonly MainWindowViewModel mainWindowDataContext;

        private string searchParameterValue = "";
        private string searchPriceMin;
        private string searchPriceMax;
        private PrescriptionParameter selectedSortParameter;
        private PrescriptionParameter selectedSearchParameter;

        public MyICommand ShowPrescriptionCommand { get; set; }
        public MyICommand SearchPrescriptionCommand { get; set; }

        private ObservableCollection<Prescription> displayedPrescriptions;
        private ObservableCollection<Prescription> displayedSearchPrescriptions;
        private ObservableCollection<PrescriptionParameter> sortingParameters;
        private ObservableCollection<PrescriptionParameter> searchParameters;

        public enum PrescriptionParameter
        {
            Code,
            Doctor,
            Date,
            Medication,
            JMBG
        }

        public DisplayPrescriptionViewModel()
        {
            ShowPrescriptionCommand = new MyICommand(ShowSortedPrescription);
            SearchPrescriptionCommand = new MyICommand(ShowSearhedPrescription);
            _app = Application.Current as App;
            mainWindowDataContext = App.Current.MainWindow.DataContext as MainWindowViewModel;

            SortingParameters = new ObservableCollection<PrescriptionParameter>();
            addSortingParams();
            SearchParameters = new ObservableCollection<PrescriptionParameter>();
            addSearchParams();

        }


        public void ShowSortedPrescription()
        {
            // Povuci sve lekove
            var prescriptions = _app.PrescriptionController.GetAll().ToList();
            // Sortiraj na osnovu selektovanog parametra
            switch (SelectedSortParameter)
            {
                case PrescriptionParameter.Code:
                    DisplayedPrescriptions = new ObservableCollection<Prescription>(_app.PrescriptionController.SortPrescriptionByCode(prescriptions));
                    break;
                case PrescriptionParameter.Doctor:
                    DisplayedPrescriptions = new ObservableCollection<Prescription>(_app.PrescriptionController.SortPrescriptionByDoctor(prescriptions));
                    break;
                case PrescriptionParameter.Date:
                    DisplayedPrescriptions = new ObservableCollection<Prescription>(_app.PrescriptionController.SortPrescriptionByDate(prescriptions));
                    break;
                default:
                    break;
            }
        }
        public void ShowSearhedPrescription()
        {
            // Povuci sve lekove
            var prescriptions = _app.PrescriptionController.GetAll().ToList();
            // Pretrazi na osnovu selektovanog parametra i unetog teksta
            switch (SelectedSearchParameter)
            {
                case PrescriptionParameter.Code:
                    int codePart;
                    // Pokusaj parsiranje, ako je uspesno pretrazi recepte, u suprotnom vrati praznu listu
                    if (Int32.TryParse(SearchParameterValue, out codePart))
                    {
                        DisplayedSearchPrescriptions = new ObservableCollection<Prescription>(_app.PrescriptionController.SearchPrescriptionByCode(codePart));
                    }
                    else if(SearchParameterValue == "") { 
                        DisplayedSearchPrescriptions = new ObservableCollection<Prescription>(_app.PrescriptionController.GetAll());
                    }
                    else { 
                        DisplayedSearchPrescriptions = new ObservableCollection<Prescription>();
                    }
                    break;
                case PrescriptionParameter.Doctor:
                    DisplayedSearchPrescriptions = new ObservableCollection<Prescription>(_app.PrescriptionController.SearchPrescriptionByDoctor(SearchParameterValue));
                    break;
                case PrescriptionParameter.JMBG:
                    DisplayedSearchPrescriptions = new ObservableCollection<Prescription>(_app.PrescriptionController.SearchPrescriptionByJMBG(SearchParameterValue));
                    break;
                case PrescriptionParameter.Medication:
                    DisplayedSearchPrescriptions = new ObservableCollection<Prescription>(_app.PrescriptionController.SearchPrescriptionByMedication(SearchParameterValue));
                    break;
                default:
                    break;
            }
        }

        private void addSortingParams()
        {
            SortingParameters.Add(PrescriptionParameter.Code);
            SortingParameters.Add(PrescriptionParameter.Doctor);
            SortingParameters.Add(PrescriptionParameter.Date);
        }
        private void addSearchParams()
        {
            SearchParameters.Add(PrescriptionParameter.Code);
            SearchParameters.Add(PrescriptionParameter.Doctor);
            SearchParameters.Add(PrescriptionParameter.JMBG);
            SearchParameters.Add(PrescriptionParameter.Medication);
        }

        #region Getters & Setters
        public string SearchParameterValue
        {
            get { return searchParameterValue; }
            set
            {
                if (searchParameterValue != value)
                {
                    searchParameterValue = value;
                    OnPropertyChanged("SearchParameterValue");
                }
            }
        }
        public string SearchPriceMin
        {
            get { return searchPriceMin; }
            set
            {
                if (searchPriceMin != value)
                {
                    searchPriceMin = value;
                    OnPropertyChanged("SearchPriceMin");
                }
            }
        }
        public string SearchPriceMax
        {
            get { return searchPriceMax; }
            set
            {
                if (searchPriceMax != value)
                {
                    searchPriceMax = value;
                    OnPropertyChanged("SearchPriceMax");
                }
            }
        }
        public PrescriptionParameter SelectedSortParameter
        {
            get
            {
                return selectedSortParameter;
            }
            set
            {
                selectedSortParameter = value;
                OnPropertyChanged("SelectedSortParameter");
            }
        }
        public PrescriptionParameter SelectedSearchParameter
        {
            get
            {
                return selectedSearchParameter;
            }
            set
            {
                selectedSearchParameter = value;
                OnPropertyChanged("SelectedSearchParameter");
            }
        }

        public ObservableCollection<Prescription> DisplayedPrescriptions
        {
            get
            {
                return displayedPrescriptions;
            }
            set
            {
                displayedPrescriptions = value;
                OnPropertyChanged("DisplayedPrescriptions");
            }
        }
        public ObservableCollection<Prescription> DisplayedSearchPrescriptions
        {
            get
            {
                return displayedSearchPrescriptions;
            }
            set
            {
                displayedSearchPrescriptions = value;
                OnPropertyChanged("DisplayedSearchPrescriptions");
            }
        }
        public ObservableCollection<PrescriptionParameter> SortingParameters
        {
            get
            {
                return sortingParameters;
            }
            set
            {
                sortingParameters = value;
                OnPropertyChanged("SortingParameters");
            }
        }
        public ObservableCollection<PrescriptionParameter> SearchParameters
        {
            get
            {
                return searchParameters;
            }
            set
            {
                searchParameters = value;
                OnPropertyChanged("SearchParameters");
            }
        }

        #endregion
    }
}
