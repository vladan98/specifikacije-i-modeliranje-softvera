﻿using Hospital.Model;
using Hospital.ViewModel.Util;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;

namespace Hospital.ViewModel
{
    class PurchaseViewModel : BindableBase
    {
        private readonly App _app;
        private readonly string CURRENCY = "din";

        // Prodaja *****
        // TextBox vrednosti
        private string purchaseStatus = "";
        private string cartStatus = "";
        private string medicationCodeValue = "";
        private string prescriptionCodeValue = "";
        // Komande
        public MyICommand AddMedicationCommand { get; set; }
        public MyICommand AddFromPrescriptionCommand { get; set; }
        // Korpa *******
        // Podaci iz listi
        private ObservableCollection<MedicationWithSellInfo> cart;
        // Komande
        public MyICommand FinishPurchaseCommand { get; set; }
        public MyICommand CancelPurchaseCommand { get; set; }
        // TextBox vrednosti
        private string sumPrice;
        // Konstruktor
        public PurchaseViewModel()
        {
            _app = Application.Current as App;
            // Prodaja
            AddMedicationCommand = new MyICommand(AddMedication);
            AddFromPrescriptionCommand = new MyICommand(AddFromPrescription);
            // Korpa
            FinishPurchaseCommand = new MyICommand(FinishPurchase);
            CancelPurchaseCommand = new MyICommand(CancelPurchase);
            Cart = new ObservableCollection<MedicationWithSellInfo>();
        }

        // Prodaja **********
        // Dodaj lek
        public void AddMedication() {
            Medication medToAdd = _app.MedicationController.GetOneByCode(MedicationCodeValue, _app.LoggedUser);
                // Ako postoji lek sa prosledjenom sifrom dodaj ga u korpu 
            if (medToAdd != null) 
            { 
                // Proveri da li je za lek potreban recept
                if (!medToAdd.Prescription) {
                    AddMedicationToCart(medToAdd); 
                    // Obavesti da je lek dodat
                    PurchaseStatus = "Medication added to cart";
                    MedicationCodeValue = "";
                } else { PurchaseStatus = "You are not allowed to sell this medication without prescription."; }
            }
            // Obavesti da lek nije pronadjen
            else { PurchaseStatus = "Medication code invalid"; }
        }

        // Dodaj lekove iz recepta
        public void AddFromPrescription()
        {            
            // Probaj parsiranje unetog koda, ako nije broj, vrati gresku
            if(Int32.TryParse(PrescriptionCodeValue, out int result))
            {
                // Ako je kod dobar pronadji recept i vrati lekove
                var medications = _app.PrescriptionController.GetAllMedicationFromPrescription(result);
                if (medications != null)
                {
                    List<Medication> medicationsToAdd = new List<Medication>(medications);
                    // Ubaci lekove u korpu ako postoje, u suprotnom prikazi gresku
                    if (medicationsToAdd != null) { 
                        medicationsToAdd.ForEach(medToAdd => AddMedicationToCart(medToAdd)); 
                        PurchaseStatus = "Medications from prescription added to cart.";
                        PrescriptionCodeValue = "";
                    }
                    else { PurchaseStatus = "Prescription code invalid."; }
                } else { PurchaseStatus = "Prescription with that code doesn't exist."; }
            } else { PurchaseStatus = "Prescription code must be number."; }
        }

        // Cart **********
        // Zavrsi sa kupovinom i napravi racun
        public void FinishPurchase() {
            // kreiraj racun
            var medicationAmount = new Dictionary<string, int>();
            Cart.ToList().ForEach(med => medicationAmount.Add(med.Code,med.Sold));
            Bill bill = new Bill
            {
                Pharmacist = _app.LoggedUser.Username,
                MedicationAmount = medicationAmount,
                SumPrice = CalculateSumPrice()
            };
            // Unesi racun
            _app.BillController.Create(_app.LoggedUser,bill);
            // Isprazni korpu
            Cart = new ObservableCollection<MedicationWithSellInfo>();
            CartStatus = "Bill Created";
            // Ozvezi ukupnu cenu
            RefreshSumPrice();
        }

        // Prekini kupovinu
        public void CancelPurchase()
        {
            // Isprazni korpu
            Cart = new ObservableCollection<MedicationWithSellInfo>();
            CartStatus = "Cart cleared";
            // Ozvezi ukupnu cenu
            RefreshSumPrice();
        }

        // Osvezi ukupnu cenu
        public void RefreshSumPrice()
        {            
            SumPrice = "Total: " + CalculateSumPrice().ToString() + " " + CURRENCY;
        }

        // Izracunaj ukupnu cenu
        public float CalculateSumPrice()
        {
            float sum = 0;
            Cart.ToList().ForEach(m => sum += m.Price * m.Sold);
            return sum;
        }

        // Dodaj lek u korpu
        private void AddMedicationToCart(Medication medToAdd)
        {
            // Ubaci sadrzaj korpe u listu
            List<MedicationWithSellInfo> medications = new List<MedicationWithSellInfo>(Cart);
            // Proveri da li lek vec postoji
            int medicationIndex = medications.FindIndex(med => med.Code == medToAdd.Code);
            // Ako lek vec postoji u korpi uvecaj vrednost za jedan
            if (medicationIndex >= 0)
            {
                medications[medicationIndex].Sold += 1;
            }else
            {
                // napravi novi lek sa informacijama o prodaji
                MedicationWithSellInfo med = new MedicationWithSellInfo(medToAdd.Code, medToAdd.Name, medToAdd.Manufacturer, medToAdd.Prescription, medToAdd.Price, 1);
                // dodaj u listu
                medications.Add(med);
            }
            // Ubaci listu u korpu
            Cart = new ObservableCollection<MedicationWithSellInfo>(medications);
            // Osvezi ukupnu cenu
            RefreshSumPrice();
        }


        #region Getters & Setters *****************
        public string MedicationCodeValue
        {
            get { return medicationCodeValue; }
            set
            {
                if (medicationCodeValue != value)
                {
                    medicationCodeValue = value;
                    OnPropertyChanged("MedicationCodeValue");
                }
            }
        }
        public string PrescriptionCodeValue
        {
            get { return prescriptionCodeValue; }
            set
            {
                if (prescriptionCodeValue != value)
                {
                    prescriptionCodeValue = value;
                    OnPropertyChanged("PrescriptionCodeValue");
                }
            }
        }
        public string CartStatus
        {
            get { return cartStatus; }
            set
            {
                if (cartStatus != value)
                {
                    cartStatus = value;
                    OnPropertyChanged("CartStatus");
                }
            }
        }
        public string PurchaseStatus
        {
            get { return purchaseStatus; }
            set
            {
                if (purchaseStatus != value)
                {
                    purchaseStatus = value;
                    OnPropertyChanged("PurchaseStatus");
                }
            }
        }
        public string SumPrice
        {
            get { return sumPrice; }
            set
            {
                if (sumPrice != value)
                {
                    sumPrice = value;
                    OnPropertyChanged("SumPrice");
                }
            }
        }

        public ObservableCollection<MedicationWithSellInfo> Cart
        {
            get
            {
                return cart;
            }
            set
            {
                cart = value;
                OnPropertyChanged("Cart");
            }
        }

        #endregion
    }
}
