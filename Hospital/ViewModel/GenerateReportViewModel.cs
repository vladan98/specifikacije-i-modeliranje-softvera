﻿using Hospital.Model;
using Hospital.ViewModel.Util;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Windows;

namespace Hospital.ViewModel
{
    class GenerateReportViewModel:BindableBase
    {
        private readonly App _app;

        private string selectedManufacturerValue;
        private User selectedPharmacistValue;
        private FilterParameter selectedFilterParameter;
        private bool byManufacturer;
        private bool byPharmacist;

        public MyICommand GenerateReportCommand { get; set; }

        private ObservableCollection<MedicationWithSellInfo> displayedMedications;
        private ObservableCollection<FilterParameter> filterParameters;
        private ObservableCollection<User> pharmacistParameters;
        private ObservableCollection<string> manufacturerParameters;

        public enum FilterParameter
        {
            All,
            Manufacturer,
            Pharmacists,
        }

        public GenerateReportViewModel()
        {
            GenerateReportCommand = new MyICommand(ShowSortedMedication);
            _app = Application.Current as App;

            FilterParameters = new ObservableCollection<FilterParameter>();
            ManufacturerParameters = new ObservableCollection<string>();
            PharmacistParameters = new ObservableCollection<User>();

            FillFilterParameterCombo();
            FillPharmacistCombo();
            FillManufacturerCombo();
        }

        public void FillFilterParameterCombo() {
            FilterParameters.Add(FilterParameter.All);
            FilterParameters.Add(FilterParameter.Manufacturer);
            FilterParameters.Add(FilterParameter.Pharmacists);
        }
        // Dodaj proizvodjace u combo
        public void FillManufacturerCombo() {
            ManufacturerParameters = new ObservableCollection<string>(_app.MedicationController.GetAllManufacturers());
        }
        // Dodaj fatmaceute u combo
        public void FillPharmacistCombo() {
            PharmacistParameters = new ObservableCollection<User>();
            var response = _app.UserController.GetAllPharmacists(_app.LoggedUser);
            if(!(response is string))
            {
                ((IEnumerable<User>)response).ToList().ForEach(pharmacist => PharmacistParameters.Add(pharmacist));
            }
        }


        public void ShowSortedMedication()
        {
            // Sortiraj na osnovu selektovanog parametra
            object response;
            switch (SelectedFilterParameter)
            {
                case FilterParameter.All:
                    response = _app.BillController.GetAllMedicationWithSellInfo(_app.LoggedUser);
                    if (!(response is string)) { 
                        var medicationWithSellInfos = (List<MedicationWithSellInfo>)response;
                        DisplayedMedications = new ObservableCollection<MedicationWithSellInfo>(medicationWithSellInfos);
                    }                    
                    break;
                case FilterParameter.Manufacturer:
                    response = _app.BillController.GetMedicationWithSellInfoByManufacturer(SelectedManufacturerValue,_app.LoggedUser);
                    if (!(response is string))
                    {
                        var medicationWithSellInfos = (List<MedicationWithSellInfo>)response;
                        DisplayedMedications = new ObservableCollection<MedicationWithSellInfo>(medicationWithSellInfos);
                    }
                    break;
                case FilterParameter.Pharmacists:
                    response = _app.BillController.GetMedicationWithSellInfoByPharmacist(SelectedPharmacistValue, _app.LoggedUser);
                    if (!(response is string))
                    {
                        var medicationWithSellInfos = (List<MedicationWithSellInfo>)response;
                        DisplayedMedications = new ObservableCollection<MedicationWithSellInfo>(medicationWithSellInfos);
                    }
                    break;
                default:
                    break;
            }
        }


        #region Getters & Setters **********************************
        public FilterParameter SelectedFilterParameter
        {
            get
            {
                return selectedFilterParameter;
            }
            set
            {
                selectedFilterParameter = value;
                OnPropertyChanged("SelectedFilterParameter");
                if (SelectedFilterParameter == FilterParameter.Pharmacists) { ByPharmacist = true; ByManufacturer = false; }
                if (SelectedFilterParameter == FilterParameter.Manufacturer) { ByPharmacist = false; ByManufacturer = true; }
            }
        }
        public User SelectedPharmacistValue
        {
            get
            {
                return selectedPharmacistValue;
            }
            set
            {
                selectedPharmacistValue = value;
                OnPropertyChanged("SelectedPharmacistValue");
            }
        }
        public string SelectedManufacturerValue
        {
            get
            {
                return selectedManufacturerValue;
            }
            set
            {
                selectedManufacturerValue = value;
                OnPropertyChanged("SelectedManufacturerValue");
            }
        }
        public bool ByPharmacist
        {
            get
            {
                return byPharmacist;
            }
            set
            {
                byPharmacist = value;
                OnPropertyChanged("ByPharmacist");
            }
        }
        public bool ByManufacturer
        {
            get
            {
                return byManufacturer;
            }
            set
            {
                byManufacturer = value;
                OnPropertyChanged("ByManufacturer");
            }
        }

        public ObservableCollection<FilterParameter> FilterParameters
        {
            get
            {
                return filterParameters;
            }
            set
            {
                filterParameters = value;
                OnPropertyChanged("filterParameters");
            }
        }
        public ObservableCollection<MedicationWithSellInfo> DisplayedMedications
        {
            get
            {
                return displayedMedications;
            }
            set
            {
                displayedMedications = value;
                OnPropertyChanged("DisplayedMedications");
            }
        }
        public ObservableCollection<string> ManufacturerParameters
        {
            get
            {
                return manufacturerParameters;
            }
            set
            {
                manufacturerParameters = value;
                OnPropertyChanged("ManufacturerParameters");
            }
        }
        public ObservableCollection<User> PharmacistParameters
        {
            get
            {
                return pharmacistParameters;
            }
            set
            {
                pharmacistParameters = value;
                OnPropertyChanged("PharmacistParameters");
            }
        }

        #endregion
    }
}
