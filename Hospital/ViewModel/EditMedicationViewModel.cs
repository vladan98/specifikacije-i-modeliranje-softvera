﻿using Hospital.Model;
using Hospital.ViewModel.Util;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Windows;

namespace Hospital.ViewModel
{
    class EditMedicationViewModel : BindableBase
    {
        private readonly App _app;

        // Dodavanje lekova *****
        // Vrednosti kontrola
        private string addingStatus = "";
        private string codeValue = "";
        private string nameValue = "";
        private string manufacturerValue = "";
        private string priceValue = "";
        private bool prescriptionValue;
        // Izmena lekova *****
        // Vrednosti kontrola
        private string editingStatus= "";
        private string editCodeValue = "";
        private string editNameValue = "";
        private string editManufacturerValue = "";
        private string editPriceValue="";
        private bool editPrescriptionValue;
        // Brisanje lekova *****
        // Vrednosti kontrola
        private string deleteStatus = "";
        private string deleteCodeValue = "";
        // Komande
        public MyICommand AddMedicationCommand { get; set; }
        public MyICommand EditMedicationCommand { get; set; }
        public MyICommand DeleteMedicationCommand { get; set; }
        // Korpa *******
        // Podaci iz listi
        // Konstruktor
        public EditMedicationViewModel()
        {
            _app = Application.Current as App;
            // Dodavanje 
            AddMedicationCommand = new MyICommand(AddMedication);
            // Izmena
            EditMedicationCommand = new MyICommand(EditMedication);
            // Brisanje
            DeleteMedicationCommand = new MyICommand(DeleteMedication);
        }

        // Dodavanje leka **********
        public void AddMedication()
        {
            Medication medToAdd = _app.MedicationController.GetOneByCode(CodeValue, _app.LoggedUser);
            // Ako ne postoji lek sa prosledjenom sifrom, omoguci dalji rad, u suprotnom obavesti
            if (medToAdd == null)
            {
                if(Single.TryParse(PriceValue, out float medPrice))
                {
                    medToAdd = new Medication
                    {
                        Code = CodeValue,
                        Name = NameValue,
                        Manufacturer = ManufacturerValue,
                        Price = medPrice,
                        Deleted = false
                    };
                    _app.MedicationController.Create(medToAdd,_app.LoggedUser);
                    // Obavesti da je lek dodat
                    AddingStatus = "Medication added";
                    EmptyAddTextBoxes();
                }else { AddingStatus = "Price has to be a number."; }
            }
            // Obavesti da lek nije pronadjen
            else { AddingStatus = "Medication with with this code already exist"; }
        }

        // Izmena leka **********
        public void EditMedication()
        {
            Medication medToUpdate = _app.MedicationController.GetOneByCode(EditCodeValue, _app.LoggedUser);
            float medPrice = 0;
            // Ako postoji lek sa prosledjenom sifrom, omoguci dalji rad, u suprotnom obavesti
            if (medToUpdate != null)
            {
                if (EditPriceValue == "" || Single.TryParse(EditPriceValue, out medPrice))
                {
                    if(EditNameValue != "") medToUpdate.Name = EditNameValue;
                    if(EditManufacturerValue != "") medToUpdate.Manufacturer = EditManufacturerValue;
                    if(EditPriceValue != "") medToUpdate.Price = medPrice;
                    if(EditManufacturerValue!="") medToUpdate.Manufacturer = EditManufacturerValue;
                    medToUpdate.Prescription = EditPrescriptionValue;
                    _app.MedicationController.Update(medToUpdate, _app.LoggedUser);
                    // Obavesti da je lek dodat
                    EditingStatus = "Medication updated";
                    EmptyEditTextBoxes();
                }
                else { EditingStatus = "Price has to be number."; }
            }
            else { EditingStatus = "Medication with that code doesn't exist."; }            
        }

        // Cart **********
        // Zavrsi sa kupovinom i napravi racun
        public void DeleteMedication()
        {
            Medication medToDelete = _app.MedicationController.GetOneByCode(DeleteCodeValue, _app.LoggedUser);
            // Ako postoji lek sa prosledjenom sifrom, omoguci dalji rad, u suprotnom obavesti
            if (medToDelete != null)
            {
                _app.MedicationController.Delete(medToDelete, _app.LoggedUser);
                // Obavesti da je lek obrisan
                DeleteStatus = "Medication deleted";
                DeleteCodeValue = "";
            }
            else { DeleteStatus = "Medication with that code doesn't exist."; }
        }


        public void EmptyAddTextBoxes()
        {
            CodeValue = "";
            NameValue = "";
            ManufacturerValue = "";
            PriceValue = "";
            PrescriptionValue = false;
        }
        
        public void EmptyEditTextBoxes()
        {
            EditCodeValue = "";
            EditNameValue = "";
            EditManufacturerValue = "";
            EditPriceValue = "";
            EditPrescriptionValue = false;
        }

        #region Getters & Setters *****************
        public string DeleteStatus{
            get { return deleteStatus; }
            set
            {
                if (deleteStatus != value)
                {
                    deleteStatus = value;
                    OnPropertyChanged("DeleteStatus");
                }
            }
        }
        public string DeleteCodeValue
        {
            get { return deleteCodeValue; }
            set
            {
                if (deleteCodeValue != value)
                {
                    deleteCodeValue = value;
                    OnPropertyChanged("DeleteCodeValue");
                }
            }
        }
        public string EditingStatus{
            get { return editingStatus; }
            set
            {
                if (editingStatus != value)
                {
                    editingStatus = value;
                    OnPropertyChanged("EditingStatus");
                }
            }
        }
        public string EditCodeValue
        {
            get { return editCodeValue; }
            set
            {
                if (editCodeValue != value)
                {
                    editCodeValue = value;
                    OnPropertyChanged("EditCodeValue");
                }
            }
        }
        public string CodeValue
        {
            get { return codeValue; }
            set
            {
                if (codeValue != value)
                {
                    codeValue = value;
                    OnPropertyChanged("CodeValue");
                }
            }
        }
        public bool EditPrescriptionValue
        {
            get { return editPrescriptionValue; }
            set
            {
                if (editPrescriptionValue != value)
                {
                    editPrescriptionValue = value;
                    OnPropertyChanged("EditPrescriptionValue");
                }
            }
        }
        public bool PrescriptionValue
        {
            get { return prescriptionValue; }
            set
            {
                if (prescriptionValue != value)
                {
                    prescriptionValue = value;
                    OnPropertyChanged("PrescriptionValue");
                }
            }
        }
        public string EditPriceValue
        {
            get { return editPriceValue; }
            set
            {
                if (editPriceValue != value)
                {
                    editPriceValue = value;
                    OnPropertyChanged("EditPriceValue");
                }
            }
        }
        public string PriceValue
        {
            get { return priceValue; }
            set
            {
                if (priceValue != value)
                {
                    priceValue = value;
                    OnPropertyChanged("PriceValue");
                }
            }
        }
        public string EditManufacturerValue
        {
            get { return editManufacturerValue; }
            set
            {
                if (editManufacturerValue != value)
                {
                    editManufacturerValue = value;
                    OnPropertyChanged("EditManufacturerValue");
                }
            }
        }
        public string ManufacturerValue
        {
            get { return manufacturerValue; }
            set
            {
                if (manufacturerValue != value)
                {
                    manufacturerValue = value;
                    OnPropertyChanged("ManufacturerValue");
                }
            }
        }
        public string NameValue
        {
            get { return nameValue; }
            set
            {
                if (nameValue != value)
                {
                    nameValue = value;
                    OnPropertyChanged("NameValue");
                }
            }
        }
        public string EditNameValue
        {
            get { return editNameValue; }
            set
            {
                if (editNameValue != value)
                {
                    editNameValue = value;
                    OnPropertyChanged("EditNameValue");
                }
            }
        }
        public string AddingStatus
        {
            get { return addingStatus; }
            set
            {
                if (addingStatus != value)
                {
                    addingStatus = value;
                    OnPropertyChanged("AddingStatus");
                }
            }
        }


        #endregion
    }
}
