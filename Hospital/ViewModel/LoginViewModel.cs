﻿using Hospital.Model;
using Hospital.Model.Enum;
using Hospital.Model.Util;
using Hospital.Service;
using Hospital.ViewModel.Util;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters;
using System.Text;
using System.Windows;
using System.Windows.Controls;

namespace Hospital.ViewModel
{
    class LoginViewModel : BindableBase
    {
        private readonly MainWindowViewModel mainWindowDataContext;
        private readonly App _app;
        private string loginErrorText;
        private string emailText = "";
        private int loginCount;
        public MyICommandWithPassword LoginCommand { get; set; }

        public LoginViewModel()
        {
            _app = Application.Current as App;
            mainWindowDataContext = App.Current.MainWindow.DataContext as MainWindowViewModel;
            LoginCount = 0;
            LoginErrorText = "";
            LoginCommand = new MyICommandWithPassword(TryLogin, CanLogin);
        }
        private bool CanLogin()
        {
            if (EmailText.Length < 1) return false;
            else return true;
        }

        public void TryLogin(PasswordBox parameter)
        {
            LoginCount++;
            object response = _app.UserController.TryLogin(EmailText, parameter.Password);
            if (response is string)
            {
                LoginErrorText = response.ToString();
                if (LoginCount == 3) App.Current.Shutdown();
            }
            else
            {
                LoginErrorText = "ok";
                _app.LoggedUser = (User)response;
                mainWindowDataContext.LoginSuccess();
                mainWindowDataContext.OnNav(NavigationDestination.MAIN_MENU);
            }

        }

        #region Getters & Setters

        public string LoginErrorText
        {
            get { return loginErrorText; }
            set
            {
                if (loginErrorText != value)
                {
                    loginErrorText = value;
                    OnPropertyChanged("LoginErrorText");
                }
            }
        }
        public int LoginCount
        {
            get { return loginCount; }
            set
            {
                if (loginCount != value)
                {
                    loginCount = value;
                    OnPropertyChanged("LoginCount");
                }
            }
        }
        public string EmailText
        {
            get { return emailText; }
            set
            {
                if (emailText != value)
                {
                    emailText = value;
                    OnPropertyChanged("EmailText");
                    LoginCommand.RaiseCanExecuteChanged();
                }
            }
        }
        #endregion
    }
}
