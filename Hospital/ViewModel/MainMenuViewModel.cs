﻿using Hospital.Model.Enum;
using Hospital.ViewModel.Util;
using System.Diagnostics;
using System.Windows;

namespace Hospital.ViewModel
{
    class MainMenuViewModel : BindableBase
    {
        private readonly App _app;
        private readonly MainWindowViewModel mainWindowDataContext;

        // vrednosti za prikaz dugmica glavnog menija
        private bool isAdmin;
        private bool isAllowedToEditMedication;
        private bool isAllowedToCreatePrescription;

        // Komande glavnog menija
        public MyICommand ShowMedicationCommand{ get; set; }
        public MyICommand EditMedicationCommand { get; set; }
        public MyICommand ShowPersctiptionCommand { get; set; }
        public MyICommand AddPrescriptionCommand { get; set; }
        public MyICommand RegisterUserCommand { get; set; }
        public MyICommand ShowUserCommand { get; set; }
        public MyICommand GenerateReportCommand { get; set; }

        public MainMenuViewModel()
        {

            _app = Application.Current as App;
            mainWindowDataContext = App.Current.MainWindow.DataContext as MainWindowViewModel;
            // Komande glavnog menija
            ShowMedicationCommand = new MyICommand(ShowMedication);
            EditMedicationCommand = new MyICommand(EditMedication);
            ShowPersctiptionCommand = new MyICommand(ShowPersctiption);
            AddPrescriptionCommand = new MyICommand(AddPrescription);
            RegisterUserCommand = new MyICommand(RegisterUser);
            ShowUserCommand = new MyICommand(ShowUser);
            GenerateReportCommand = new MyICommand(GenerateReport);

            // vrednosti za prikaz dugmica glavnog menija
            IsAdmin = _app.LoggedUser.UserType == UserType.Administrator;
            IsAllowedToEditMedication = _app.LoggedUser.UserType == UserType.Administrator || _app.LoggedUser.UserType == UserType.Pharmacist;
            IsAllowedToCreatePrescription = _app.LoggedUser.UserType == UserType.Doctor;
        }

        public void ShowMedication()
        {
            mainWindowDataContext.OnNav(NavigationDestination.DISPLAY_MEDICATION);
        }
        public void EditMedication()
        {
            mainWindowDataContext.OnNav(NavigationDestination.EDIT_MEDICATION);
        }        
        public void ShowPersctiption()
        {
            mainWindowDataContext.OnNav(NavigationDestination.DISPLAY_PRESCRIPTION);
        }
        public void AddPrescription()
        {
            mainWindowDataContext.OnNav(NavigationDestination.ADD_PRESCRIPTION);
        }
        public void ShowUser()
        {
            mainWindowDataContext.OnNav(NavigationDestination.DISPLAY_USER);
        }
        public void GenerateReport()
        {
            mainWindowDataContext.OnNav(NavigationDestination.GENERATE_REPORT);
        }
        public void RegisterUser()
        {
            mainWindowDataContext.OnNav(NavigationDestination.REGISTER_USER);
        }

        #region Getters & Setters
        public bool IsAllowedToCreatePrescription
        {
            get { return isAllowedToCreatePrescription; }
            set
            {
                if (isAllowedToCreatePrescription != value)
                {
                    isAllowedToCreatePrescription = value;
                    OnPropertyChanged("IsAllowedToCreatePrescription");
                }
            }
        }
        public bool IsAllowedToEditMedication
        {
            get { return isAllowedToEditMedication; }
            set
            {
                if (isAllowedToEditMedication != value)
                {
                    isAllowedToEditMedication = value;
                    OnPropertyChanged("IsAllowedToEditMedication");
                }
            }
        }
        public bool IsAdmin
        {
            get { return isAdmin; }
            set
            {
                if (isAdmin != value)
                {
                    isAdmin = value;
                    OnPropertyChanged("IsAdmin");
                }
            }
        }

        #endregion
    }
}
