﻿using Hospital.Model;
using Hospital.ViewModel.Util;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;

namespace Hospital.ViewModel
{
    
    class DisplayUserViewModel : BindableBase
    {
        private readonly App _app;
        private readonly MainWindowViewModel mainWindowDataContext;

        private UserSortParameter selectedSortParameter;

        public MyICommand ShowUsersCommand { get; set; }

        private ObservableCollection<User> displayedUsers;
        private ObservableCollection<UserSortParameter> sortingParameters;

        public enum UserSortParameter
        {
            Name,
            LastName,
            Role
        }

        public DisplayUserViewModel()
        {
            ShowUsersCommand = new MyICommand(ShowSortedPrescription);
            _app = Application.Current as App;
            mainWindowDataContext = App.Current.MainWindow.DataContext as MainWindowViewModel;

            SortingParameters = new ObservableCollection<UserSortParameter>();
            addSortingParams();

        }


        public void ShowSortedPrescription()
        {
            // Povuci sve lekove
            var users = new List<User>();
            var response = _app.UserController.GetAll(_app.LoggedUser);
            if(response is IEnumerable<User>)
            {
                users = ((IEnumerable<User>)response).ToList();
            }
            // Sortiraj na osnovu selektovanog parametra
            switch (SelectedSortParameter)
            {
                case UserSortParameter.Name:
                    DisplayedUsers = new ObservableCollection<User>(_app.UserController.SortUsersByName(users));
                    break;
                case UserSortParameter.LastName:
                    DisplayedUsers = new ObservableCollection<User>(_app.UserController.SortUsersByLastName(users));
                    break;
                case UserSortParameter.Role:
                    DisplayedUsers = new ObservableCollection<User>(_app.UserController.SortUsersByRole(users));
                    break;
                default:
                    break;
            }
        }

        private void addSortingParams()
        {
            SortingParameters.Add(UserSortParameter.Name);
            SortingParameters.Add(UserSortParameter.LastName);
            SortingParameters.Add(UserSortParameter.Role);
        }

        #region Getters & Setters
        public UserSortParameter SelectedSortParameter
        {
            get
            {
                return selectedSortParameter;
            }
            set
            {
                selectedSortParameter = value;
                OnPropertyChanged("SelectedSortParameter");
            }
        }

        public ObservableCollection<User> DisplayedUsers
        {
            get
            {
                return displayedUsers;
            }
            set
            {
                displayedUsers = value;
                OnPropertyChanged("DisplayedUsers");
            }
        }
        public ObservableCollection<UserSortParameter> SortingParameters
        {
            get
            {
                return sortingParameters;
            }
            set
            {
                sortingParameters = value;
                OnPropertyChanged("SortingParameters");
            }
        }

        #endregion
    }
}
