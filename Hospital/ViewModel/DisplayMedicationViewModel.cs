﻿using Hospital.Model;
using Hospital.ViewModel.Util;
using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Windows;

namespace Hospital.ViewModel
{
    class DisplayMedicationViewModel : BindableBase
    {
        private readonly App _app;

        private string searchParameterValue ="";
        private string searchPriceMin;
        private string searchPriceMax;
        private MedicationParameter selectedSortParameter;
        private MedicationParameter selectedSearchParameter;

        public MyICommand ShowMedicationCommand { get; set; }
        public MyICommand SearchMedicationCommand { get; set; }

        private ObservableCollection<Medication> displayedMedications;
        private ObservableCollection<Medication> displayedSearchMedications;
        private ObservableCollection<MedicationParameter> sortingParameters;
        private ObservableCollection<MedicationParameter> searchParameters;

        public enum MedicationParameter
        {
            Name,
            Manufacturer,
            Price,
            Code
        }

        public DisplayMedicationViewModel()
        {
            ShowMedicationCommand = new MyICommand(ShowSortedMedication);
            SearchMedicationCommand = new MyICommand(ShowSearhedMedication);
            _app = Application.Current as App;

            SortingParameters = new ObservableCollection<MedicationParameter>();
            AddSortingParams();
            SearchParameters = new ObservableCollection<MedicationParameter>();
            AddSearchParams();
            
        }


        public void ShowSortedMedication()
        {
            // Povuci sve lekove
            var medications = _app.MedicationController.GetAll(_app.LoggedUser).ToList();
            // Sortiraj na osnovu selektovanog parametra
            switch (SelectedSortParameter)
            {
                case MedicationParameter.Name:
                    DisplayedMedications = new ObservableCollection<Medication>(_app.MedicationController.SortMedicationByName(medications));
                    break;
                case MedicationParameter.Manufacturer:
                    DisplayedMedications = new ObservableCollection<Medication>(_app.MedicationController.SortMedicationByManufacturer(medications));
                    break;
                case MedicationParameter.Price:
                    DisplayedMedications = new ObservableCollection<Medication>(_app.MedicationController.SortMedicationByPrice(medications));
                    break;
                default:
                    break;
            }
        }
        public void ShowSearhedMedication()
        {
            // Pretrazi na osnovu selektovanog parametra i unetog teksta
            switch (SelectedSearchParameter)
            {
                case MedicationParameter.Code:
                    DisplayedSearchMedications = new ObservableCollection<Medication>(_app.MedicationController.SearchMedicationByCode(SearchParameterValue, _app.LoggedUser));
                    break;
                case MedicationParameter.Name:
                    DisplayedSearchMedications = new ObservableCollection<Medication>(_app.MedicationController.SearchMedicationByName(SearchParameterValue, _app.LoggedUser));
                    break;
                case MedicationParameter.Manufacturer:
                    DisplayedSearchMedications = new ObservableCollection<Medication>(_app.MedicationController.SearchMedicationByManufacturer(SearchParameterValue, _app.LoggedUser));
                    break;
                case MedicationParameter.Price:
                    float priceMin, priceMax;
                    // Pokusaj parsiranje, ako je uspesno pretrazi lekove, u suprotnom ne radi nista
                    if (Single.TryParse(SearchPriceMin, out priceMin) && Single.TryParse(SearchPriceMax, out priceMax)) { 
                        if (SearchPriceMin == "") priceMin = 0;
                        DisplayedSearchMedications = new ObservableCollection<Medication>(_app.MedicationController.SearchMedicationByPriceRange(priceMin, priceMax, _app.LoggedUser)); 
                    }
                    break;
                default:
                    break;
            }
        }

        private void AddSortingParams()
        {
            SortingParameters.Add(MedicationParameter.Name);
            SortingParameters.Add(MedicationParameter.Manufacturer);
            SortingParameters.Add(MedicationParameter.Price);
        }
        private void AddSearchParams()
        {
            SearchParameters.Add(MedicationParameter.Code);
            SearchParameters.Add(MedicationParameter.Name);
            SearchParameters.Add(MedicationParameter.Manufacturer);
            SearchParameters.Add(MedicationParameter.Price);
        }

        #region Getters & Setters
        public string SearchParameterValue
        {
            get { return searchParameterValue; }
            set
            {
                if (searchParameterValue != value)
                {
                    searchParameterValue = value;
                    OnPropertyChanged("SearchParameterValue");
                }
            }
        }
        public string SearchPriceMin
        {
            get { return searchPriceMin; }
            set
            {
                if (searchPriceMin != value)
                {
                    searchPriceMin = value;
                    OnPropertyChanged("SearchPriceMin");
                }
            }
        }
        public string SearchPriceMax
        {
            get { return searchPriceMax; }
            set
            {
                if (searchPriceMax != value)
                {
                    searchPriceMax = value;
                    OnPropertyChanged("SearchPriceMax");
                }
            }
        }
        public MedicationParameter SelectedSortParameter
        {
            get
            {
                return selectedSortParameter;
            }
            set
            {
                selectedSortParameter = value;
                OnPropertyChanged("SelectedSortParameter");
            }
        }
        public MedicationParameter SelectedSearchParameter
        {
            get
            {
                return selectedSearchParameter;
            }
            set
            {
                selectedSearchParameter = value;
                OnPropertyChanged("SelectedSearchParameter");
            }
        }

        public ObservableCollection<Medication> DisplayedMedications
        {
            get
            {
                return displayedMedications;
            }
            set
            {
                displayedMedications = value;
                OnPropertyChanged("DisplayedMedications");
            }
        }
        public ObservableCollection<Medication> DisplayedSearchMedications
        {
            get
            {
                return displayedSearchMedications;
            }
            set
            {
                displayedSearchMedications = value;
                OnPropertyChanged("DisplayedSearchMedications");
            }
        }
        public ObservableCollection<MedicationParameter> SortingParameters
        {
            get
            {
                return sortingParameters;
            }
            set
            {
                sortingParameters = value;
                OnPropertyChanged("SortingParameters");
            }
        }
        public ObservableCollection<MedicationParameter> SearchParameters
        {
            get
            {
                return searchParameters;
            }
            set
            {
                searchParameters = value;
                OnPropertyChanged("SearchParameters");
            }
        }

        #endregion
    }
}
