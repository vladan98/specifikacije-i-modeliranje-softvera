﻿using Hospital.Model;
using Hospital.ViewModel.Util;
using System;
using System.Collections.Generic;
using System.Windows;

namespace Hospital.ViewModel
{
    class AddPrescriptionViewModel : BindableBase
    {
        private readonly App _app;

        // Dodavanje lekova *****
        // Vrednosti kontrola
        private string addingMedicationStatus = "";
        private string addingPrescriptionStatus = "";
        private string patientJMBGValue = "";
        private DateTime prescriptionTimeValue;
        private string medicationCodeValue = "";
        private string medicationQuantityValue = "";
        // Komande
        public MyICommand AddMedicationCommand { get; set; }
        public MyICommand AddPrescriptionCommand { get; set; }
        // Podaci o lekovima
        Dictionary<string, int> medicationAmount;
        // Konstruktor
        public AddPrescriptionViewModel()
        {
            _app = Application.Current as App;
            // Dodavanje recepta
            AddMedicationCommand = new MyICommand(AddMedication);
            // Dodavanje leka
            AddPrescriptionCommand = new MyICommand(AddPrescription);
            // Podaci o lekovima
            MedicationAmount = new Dictionary<string, int>();
            // Podaci o datumu i vremenu
            PrescriptionTimeValue = DateTime.Now;
        }

        // Dodavanje leka **********
        public void AddMedication()
        {
            Medication medToAdd = _app.MedicationController.GetOneByCode(MedicationCodeValue, _app.LoggedUser);
            // Ako postoji lek sa prosledjenom sifrom, omoguci dalji rad, u suprotnom obavesti
            if (medToAdd != null)
            {
                if(Int32.TryParse(MedicationQuantityValue, out int medQuantity) && medQuantity > 0)
                {
                    medicationAmount.Add(medToAdd.Code, medQuantity);
                    // Obavesti da je lek dodat
                    AddingMedicationStatus = "Medication added";
                    EmptyAddMedicationTextBoxes();
                }else { AddingMedicationStatus = "Medication quantity has to be a positive number."; }
            }
            // Obavesti da lek nije pronadjen
            else { AddingMedicationStatus = "Medication with with this code doesn't exist"; }
        }

        // Dodavanje recepta **********
        public void AddPrescription()
        {
            if (PatientJMBGValue != "")
            {
                if (MedicationAmount.Count != 0)
                {
                    var prescriptionToAdd = new Prescription
                    {
                        JMBG = PatientJMBGValue,
                        MedicationAmount = MedicationAmount,
                        Doctor = _app.LoggedUser.Username,
                        Date = PrescriptionTimeValue.ToString("MM/dd/yyyy H:mm")
                    };
                    _app.PrescriptionController.Create(prescriptionToAdd, _app.LoggedUser);
                    // Obavesti da je recept dodat
                    AddingPrescriptionStatus = "Prescription added";
                    ResetPrescriptionValues();
                }
                else { AddingPrescriptionStatus = "You have to add medication to prescription"; }   
            }
            else { AddingPrescriptionStatus = "You have to enter patient JMBG"; }        
        }


        public void EmptyAddMedicationTextBoxes()
        {
            MedicationCodeValue= "";
            MedicationQuantityValue = "";
        }
        
        public void ResetPrescriptionValues()
        {
            // Isprazni podatke o lekovima
            medicationAmount = new Dictionary<string, int>();
            PatientJMBGValue = "";
            MedicationCodeValue = "";
            MedicationQuantityValue = "";
            PrescriptionTimeValue = DateTime.Now;
        }

        #region Getters & Setters *****************

        public string AddingMedicationStatus
        {
            get { return addingMedicationStatus; }
            set
            {
                if (addingMedicationStatus != value)
                {
                    addingMedicationStatus = value;
                    OnPropertyChanged("AddingMedicationStatus");
                }
            }
        }
        public string AddingPrescriptionStatus
        {
            get { return addingPrescriptionStatus; }
            set
            {
                if (addingPrescriptionStatus != value)
                {
                    addingPrescriptionStatus = value;
                    OnPropertyChanged("AddingPrescriptionStatus");
                }
            }
        }
        public string PatientJMBGValue
        {
            get { return patientJMBGValue; }
            set
            {
                if (patientJMBGValue != value)
                {
                    patientJMBGValue = value;
                    OnPropertyChanged("PatientJMBGValue");
                }
            }
        }
        public DateTime PrescriptionTimeValue
        {
            get { return prescriptionTimeValue; }
            set
            {
                if (prescriptionTimeValue != value)
                {
                    prescriptionTimeValue = value;
                    OnPropertyChanged("EditingStatus");
                }
            }
        }
        public string MedicationCodeValue
        {
            get { return medicationCodeValue; }
            set
            {
                if (medicationCodeValue != value)
                {
                    medicationCodeValue = value;
                    OnPropertyChanged("MedicationCodeValue");
                }
            }
        }
        public string MedicationQuantityValue
        {
            get { return medicationQuantityValue; }
            set
            {
                if (medicationQuantityValue != value)
                {
                    medicationQuantityValue = value;
                    OnPropertyChanged("MedicationQuantityValue");
                }
            }
        }
        public Dictionary<string,int> MedicationAmount
        {
            get { return medicationAmount; }
            set
            {
                if (medicationAmount != value)
                {
                    medicationAmount = value;
                    OnPropertyChanged("MedicationQuantityValue");
                }
            }
        }


        #endregion
    }
}
