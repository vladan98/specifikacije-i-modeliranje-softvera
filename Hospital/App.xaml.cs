﻿using Hospital.Controller;
using Hospital.Model;
using Hospital.Repository;
using Hospital.Repository.JSONStream;
using Hospital.Service;
using System.Windows;

namespace Hospital
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private const string USERS_FILE = @"../../../Resources/users.json";
        private const string MEDICATION_FILE = @"../../../Resources/medications.json";
        private const string PRESCRIPTION_FILE = @"../../../Resources/prescriptions.json";
        private const string BILL_FILE = @"../../../Resources/bills.json";
        public App()
        {
            var userRepository = new UserRepository(new JSONStream<User>(USERS_FILE));
            var medicationRepository = new MedicationRepository(new JSONStream<Medication>(MEDICATION_FILE));
            var prescriptionRepository = new PrescriptionRepository(new JSONStream<Prescription>(PRESCRIPTION_FILE),medicationRepository);
            var billRepository = new BillRepository(new JSONStream<Bill>(BILL_FILE), medicationRepository);

            var userService = new UserService(userRepository);
            var medicationService = new MedicationService(medicationRepository);
            var prescriptionService = new PrescriptionService(prescriptionRepository);
            var billService = new BillService(billRepository);

            UserController = new UserController(userService);
            MedicationController = new MedicationController(medicationService);
            PrescriptionController = new PrescriptionController(prescriptionService);
            BillController = new BillController(billService);


        }
        public User LoggedUser { get; set; }

        public UserController UserController { get; private set; }
        public MedicationController MedicationController { get; private set; }
        public PrescriptionController PrescriptionController { get; private set; }
        public BillController BillController { get; private set; }


    }
}
