﻿using System.Collections.Generic;

namespace Hospital.Model
{
    public class Prescription
    {
        protected int code;
        protected string doctor;
        protected string jmbg;
        protected string date;
        protected Dictionary<string, int> medicationAmount;

        public Prescription() { }
        public Prescription(int code, string doctor, string jmbg, string date, Dictionary<string, int> medicationAmount)
        {
            this.code = code;
            this.doctor = doctor;
            this.jmbg = jmbg;
            this.date = date;
            this.medicationAmount = medicationAmount;
        }

        public int Code { get => code; set => code = value; }
        public string Doctor { get => doctor; set => doctor = value; }
        public string JMBG { get => jmbg; set => jmbg = value; }
        public string Date { get => date; set => date = value; }
        public Dictionary<string, int> MedicationAmount { get => medicationAmount; set => medicationAmount = value; }

    }
}
