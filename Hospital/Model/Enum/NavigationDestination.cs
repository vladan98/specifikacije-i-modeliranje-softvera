﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hospital.Model.Enum
{
    public enum NavigationDestination
    {
        MAIN_MENU,
        DISPLAY_MEDICATION,
        PURCHASE_MEDICATION,
        EDIT_MEDICATION,
        DISPLAY_PRESCRIPTION,
        ADD_PRESCRIPTION,
        REGISTER_USER,
        DISPLAY_USER,
        GENERATE_REPORT,
    }
}
