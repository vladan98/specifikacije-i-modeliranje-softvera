﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hospital.Model.Enum
{
    public enum UserType
    {
        Administrator,
        Doctor,
        Pharmacist
    }
}
