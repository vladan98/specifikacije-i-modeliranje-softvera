﻿using Hospital.Model.Enum;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hospital.Model
{
    public class User
    {
        protected string username;
        protected string password;
        protected string name;
        protected string lastName;
        protected UserType userType;

        public string Username { get => username; set => username = value; }
        public string Password { get => password; set => password = value; }
        public string Name { get => name; set => name = value; }
        public string LastName { get => lastName; set => lastName = value; }
        public UserType UserType { get => userType; set => userType = value; }

        public override string ToString() {
            return "name: " + name + ",username: " + username + ",userType: " + UserType.ToString();
        }
    }
}
