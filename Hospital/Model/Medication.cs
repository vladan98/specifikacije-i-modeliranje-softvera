﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace Hospital.Model
{
    public class Medication 
    {
        protected string code;
        protected string name;
        protected string manufacturer;
        protected bool prescription;
        protected bool deleted;
        protected float price;

        public Medication(string code, string name, string manufacturer, bool prescription, float price) {
            this.code = code;
            this.name = name;
            this.manufacturer = manufacturer;
            this.prescription = prescription;
            this.price = price;
        }
        public Medication(string code) {
            this.code = code;
        }
        public Medication() {
        }

        public string Code { get => code; set => code= value; }
        public string Name { get => name; set => name = value; }
        public string Manufacturer { get => manufacturer; set => manufacturer = value; }
        public bool Prescription { get => prescription; set => prescription = value; }
        public bool Deleted { get => deleted; set => deleted = value; }
        public float Price { get => price; set => price = value; }

    }
    public class MedicationWithSellInfo : Medication
    {
        protected int sold;

        public MedicationWithSellInfo() : base() { }

        public MedicationWithSellInfo(string code, string name, string manufacturer, bool prescription, float price, int sold):base(code,name,manufacturer,prescription,price) 
        {
            this.sold = sold;
        }
        public float SumPrice { get => price*sold; }
        public int Sold { get => sold; set => sold = value; }

    }
}
