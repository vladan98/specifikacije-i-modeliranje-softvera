﻿using System.Collections.Generic;

namespace Hospital.Model
{
    public class Bill
    {
        protected int code;
        protected string pharmacist;
        protected string dateTime;
        protected Dictionary<string, int> medicationAmount;
        protected float sumPrice;

        public Bill() {}
        public Bill(string pharmacist, string dateTime, Dictionary<string, int> medicationAmount, float sumPrice)
        {
            this.pharmacist = pharmacist;
            this.dateTime = dateTime;
            this.medicationAmount = medicationAmount;
            this.sumPrice = sumPrice;
        }

        public int Code { get => code; set => code = value; }
        public string Pharmacist { get => pharmacist; set => pharmacist = value; }
        public string DateTime { get => dateTime; set => dateTime = value; }
        public Dictionary<string, int> MedicationAmount { get => medicationAmount; set => medicationAmount = value; }
        public float SumPrice { get => sumPrice; set => sumPrice = value; }
    }
}
